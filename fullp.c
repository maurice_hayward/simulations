// full multipler  overflow needs work


#include <stdio.h>

#define MODULUS    2147483647L /* DON'T CHANGE THIS VALUE                   */

int find_all_multiplers(const long int m ){
	/* --------------------------------------------------------------------
 * Use this  procedure to find the number of multiplers for 
 	modulus m                   
 * --------------------------------------------------------------------
 */
	int count = 0;
	long int a;
	for(a = 1; a <= m-1 ; a++){

		long int p = 1;
		long int x = a;

		while( x != 1){
			p++;
			
			x = (a * x) % m;  //beware of overflow
		}

		if (p == m-1){count++;}  // a is a full Multipler
	}

	return count;
}

int find_first_multipler(const long int m ){
		/* --------------------------------------------------------------------
 * Use this  procedure to find the first  multipler for 
 	modulus m                   
 * --------------------------------------------------------------------
 */
	long int a;
	for(a = 1; a <= m-1 ; a++){

		long int p = 1;
		long int x = a;

		while( x != 1){
			p++;
			
			x = (a * x) % m; //beware of overflow
		}

		if (p == m-1){ return a;}  // a is a full Multipler
	}

	return 0;

}

int is_full_multipler(const long int m, const long int a ){
		/* --------------------------------------------------------------------
 * Use this  procedure to find the first  multipler for 
 	modulus m                   
 * --------------------------------------------------------------------
 */
		long int p = 1;
		long int x = a;

		while( x != 1){
			p++;
			
			x = (a * x) % m; //beware of overflow
		}

		if (p == m-1){ return a;}  // a is a full Multipler


	return 0;

}
int main(){

	int a  =  is_full_multipler(3,2);
	puts("for m = 3");
	if(a){
		puts("2 is a is_full_multipler of 3");
	}
	else{
		puts("2 is not a full multipler of 3");
	}

	return 0;
}
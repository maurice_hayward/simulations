


#include <stdio.h>
#include <math.h>
#include <string.h>
#define MAX_INTEGER32  4294967296L /* for 32 bit        */

FILE *fp;

unsigned long find_next_iteration(const unsigned long  a, const unsigned long x, const unsigned long m){
	/* Modification of Alogorthm 2.2.1 that fixes the Overflow problem and 
	no value is out of range of (0,1,..m) */

	const unsigned long q = (int) floor ( (double) m  / a);
	const unsigned long r =  m % a;
	
	const unsigned long y1 = a * (x%q);   
	const unsigned long y2 = r * (x/q);   // y(x) = y1 -y2
 
	if(y1 >= y2){
		return y1 - y2 ;  // delta(x) = 0
	}                        
	else{
		return (m - y2) + y1  ; // delta(x) = 1
	}                        

}
unsigned long count_full_multiplers(const unsigned long m ){
		/* --------------------------------------------------------------------
 * Use this  procedure to find the first  multipler for 
 	modulus m                   
 * --------------------------------------------------------------------
 */
	unsigned long a;
	unsigned long count = 0;
	for(a = 1; a <= m-1 ; a++){

		
		unsigned long p = 1;
		unsigned long x = a;

		while( (x != 1) && !(p > m)){
			p++;
			x = find_next_iteration(a,x,m);
			
		}
		
		if (p == m-1){ 
			count++;
			fprintf(fp, " a =  %lu   count = %lu\n", a, count);
		}
	}

	return count; 

}

int main(){
	
  	fp = fopen("full_mult.txt","w+");

	fprintf(fp, "There are %lu full multiplers for 2^32 - 135", count_full_multiplers(MAX_INTEGER32 - 135));

	fclose(fp);

	return 0;
}
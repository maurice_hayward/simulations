


#include <stdio.h>
#include <math.h>

#define MODULUS  32767L /* for 16 bit        */


int LargestPrime(int TheNum)
/*Find Largest Prime less than or equal to Thenum */
/* From http://www.cplusplus.com/forum/beginner/10076/ */
/* post #3 */
/* by 	arun1390 */
{
	int FactorCount = 0;
	int i;
	for ( i=TheNum; i>=2; --i)
	{
		int j;
		for ( j=2; j<sqrt(i) + 1; ++j)  // HERE
		{
			if (i % j == 0)
			{
		            ++FactorCount;
                            break;  // HERE
			}
		}

		if (FactorCount == 0)
		{
			return i;
			break;
		}

		FactorCount = 0;
	}
	return 0;
}

int gcd ( int a, int b )
/* Standard C Function: Greatest Common Divisor */
/* From http://www.math.wustl.edu/~victor/mfmm/compaa/gcd.c */
/* by 	Mladen Victor Wickerhauser -Professor of Mathematics */
/*Washingting University*/
{
  int c;
  while ( a != 0 ) {
     c = a; a = b%a;  b = c;
  }
  return b;
}

int find_next_iteration(const long int a, const long int x, const long int m){
	/* Modification of Lemhar equation that fixes the Overflow problem */

	const long int q = (int) floor ( (double) m  / a);
	const long int r =  m % a;
	
		
	const unsigned long int y1 = a * (x%q);   
	const unsigned long int y2 = r * (x/q);   // y(x) = y1 -y2


	 
	if(y1 > y2){
		return y1 - y2 ;  // delta(x) = 0
	}                        
	else{
		return (m - y2) + y1  ; // delta(x) = 1
	}                        



}

int find_first_multipler(const long int m ){
		/* --------------------------------------------------------------------
 * Use this  procedure to find the first  multipler for 
 	modulus m                   
 * --------------------------------------------------------------------
 */
	long int a;
	for(a = 1; a <= m-1 ; a++){

		long int p = 1;
		long int x = a;

		while( (x != 1) && !(p > m)){
			p++;
			
			x = find_next_iteration(a,x,m);

			
		}
		
		if (p == m-1){ return a;} 
	 // a is the first full Multipler
	
	}

	return 0; // there are no Multiplers

}

int find_first_mod_multipler( const long int m){
		/* --------------------------------------------------------------------
 * Use this  procedure to find the first modlus compatible multipler for 
 	modulus m                   
 --------------------------------------------------------------------
 */		long int a;
 		if (!(a = find_first_multipler(m))){
 			puts("a is 0");
 	
 			return 0; // there are no Multiplers
 		}
	
		long int i = 1;
		long int x = a;
		
		while( x != 1){
			if((m%x < (int)m/(int)x) && (gcd(i,m-1) == 1)){ printf("%li\n",x);}

			i++;
			x = find_next_iteration(a,x,m);
		}
	
	
	

	return 0; // there are no Multiplers

}


int main(){
	
	find_first_mod_multipler(LargestPrime(MODULUS));
	return 0;
}
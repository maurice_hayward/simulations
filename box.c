/* ------------------------------------------------------------------------- */
//box
#include <stdio.h>
#include "rng.h"

#define N 100000L                          /* number of replications */


   long Equilikely(long a, long b)        
/* ------------------------------------------------
 * generate an Equilikely random variate, use a < b 
 * ------------------------------------------------
 */
{
  return (a + (long) ((b - a + 1) * Random()));
}


  int main(void)
{ 
  int j;
  long   i;                               /* replication index      */
  long   box;                               /* Box      */
  long   ten_in_1st = 0;                /*Number the times the First comparment is $10*/
  long   ten_in_2nd = 0;          /* Number times $10 in second*/
 
 for( j = 0; j < 5; j++){ //Run the Simulations 5 times with different seeds
  
  PutSeed(-1);
  for (i = 0; i < N; i++) {
    
    //Pick box 1 , 2 , or 3
    box = Equilikely(1,3);
    

    //box with $10 in both comparments
    if(box  == 1){
     ten_in_1st++;
     ten_in_2nd++;
     }  
    
    // box with $10 and $5 bill
     // if first compartment is $10 ten_in_1st is incremented
     //otherwise ten_in_1st is not incremented
    if(box == 3){                      
      ten_in_1st = ten_in_1st + Equilikely(0,1);   
                        
    } 

  }

  
  printf("Probabily of $10 in the second if first is $10 is %f\n", (float) ten_in_2nd / ten_in_1st );
}
  return (0);
 }
    


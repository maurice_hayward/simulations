#include <stdio.h>
#include "rngs.h"
#include <math.h>

#define N 100000L                          /* number of replications */


   double Uniform(double a, double b)       
/* --------------------------------------------
 * generate a Uniform random variate, use a < b 
 * --------------------------------------------
 */
{                                         
  return (a + (b - a) * Random());    
}

int is_triangle(double x, double y , double z){
  /* Uses Triangle Inequalitly to determine if a triangle is made*/
  
  int A = (x+y) >= z;
  int B = (x+z) >= y;
  int C = (y+z) >= x;

  return A && B && C;
}

  int main(void){ 

    PlantSeeds(-1);
    SelectStream(0);
    long int i = 0;
    long int count = 1;
    
    while(i < N){

      double X = Uniform(0.0, 1.0); // Cut rope at point X
      double Y = Uniform(0.0, 1.0); // Cut rope at point Y

      if(X<Y){
        double side1 = X;
        double side2 = fabs(Y-X);
        double side3 = 1 -  Y;
      }
      else{
        double side1 = y;
        double side2 = fabs(Y-X);
        double side3 = 1 -  X;
      }

      

      if(is_triangle(side1,slide2,side3)){count++;}

    }
  
    printf("Probabily of forming a triangle is: %f\n", (float)count / N);;
    
    return (0);
 }
    


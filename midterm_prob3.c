#include <stdio.h>
#include "rngs.h"
#include <math.h>

#define N 100000L                          /* number of replications */


   long Equilikely(long a, long b)        
/* ------------------------------------------------
 * generate an Equilikely random variate, use a < b 
 * ------------------------------------------------
 */
{
  return (a + (long) ((b - a + 1) * Random()));
}

void swap(long int *a, long int i, long int j){
  long int temp = a[i];
  a[i] = a[j];
  a[j] = temp;
}

void shuffle_array(long int *a, long int N)
/*Fisher_Yates Alogrim to shuffle an Array*/
{

  int i;
  for(i=0;i < N -1, i++){
    int j =  Equilikely(0, 8);
    int t = a[j];
    swap(a, i , j);
  }

}
int check_O_win(char board[3][3] ){
  int col1 = (board[0][0] = 'o') && (board[1][0] = 'o') && (board[2][0] = 'o');
  int col2 = (board[0][1] = 'o') && (board[1][1] = 'o') && (board[2][1] = 'o');
  int col3 = (board[0][2] = 'o') && (board[1][2] = 'o') && (board[2][2] = 'o');
  int dia1 = (board[0][0] = 'o') && (board[1][1] = 'o') && (board[2][2] = 'o');
  int dia2 = (board[0][2] = 'o') && (board[1][1] = 'o') && (board[0][2] = 'o');
  return (col1 || col2 || col3 || dia1 || dia2);
}
int check_X_win(char board[3][3] ){
  int col1 = (board[0][0] = 'x') && (board[1][0] = 'x') && (board[2][0] = 'x');
  int col2 = (board[0][1] = 'x') && (board[1][1] = 'x') && (board[2][1] = 'x');
  int col3 = (board[0][2] = 'x') && (board[1][2] = 'x') && (board[2][2] = 'x');
  int dia1 = (board[0][0] = 'x') && (board[1][1] = 'x') && (board[2][2] = 'x');
  int dia2 = (board[0][2] = 'x') && (board[1][1] = 'x') && (board[0][2] = 'x');
  return (col1 || col2 || col3 || dia1 || dia2);
}

int main(void){ 
    
   
    
    int moves[9] = { 0, 1 ,2 ,3 ,4 ,5 ,6 ,7 , 8 };
    shuffle_array(moves, 9);
    char board[3][3] = {'a','a','a','a','a','a','a','a','a'};
    int i;
    for (i = 1; i <= 9; i++)
    {
      if(i%2){     //move X
        board[moves[i -1]] = 'x';
        check_X_win(board);
      }
      else{
        board[moves[i -1]] = 'o';
        check_X_win(board);
      }
    }

    
    return 0;
 }
    


